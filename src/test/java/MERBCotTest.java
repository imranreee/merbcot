import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

import static io.appium.java_client.touch.offset.PointOption.point;

public class MERBCotTest {
    WebDriver driver;

    //element id
    By cancelBtn = By.id("android:id/button2");
    By lblHipotecario = By.id("lblHipotecario");
    By lblMensajePrincipal = By.id("lblMensajePantalla");
    By flxHipotecario = By.id("flxLogoHipotecario");
    By flxDatosFinancieros = By.id("frmDatosFinancierosApp");
    By nombreApellido = By.id("txtNombreApellido");
    By lblMensaje = By.id("lblMensajeFooter");
    By generom = By.id("imgGeneroM");
    By residencia = By.id("lstResidencia");
    By previousBtn = By.id("android:id/prev");
    By selectOne = By.xpath("//*[@text='1']");
    By okBtn = By.id("button1");
    By seekBarOne = By.id("sldValorPropiedad");
    By seekBarTwo = By.id("sldAbonoInicial");
    By seekBarThree = By.id("sldPlazoInicial");
    By panameno = By.xpath("//*[@text='Panameño']");
    By cedulaPasaporte = By.id("txtCedulaPasaporte");
    By emailField = By.id("txtCorreoElectronico");
    By mobileField = By.id("txtNumeroTelefono");
    By tipoIngreso = By.id("lstTipoIngreso");
    By asalariado = By.xpath("//*[@text='Asalariado']");
    By tuLetra = By.xpath("//*[@text='Calcula tu letra']");
    By btnCalcularLetra = By.id("btnCalcularLetra");
    By valorVivienda = By.id("lblValorVivienda");
    By calenderBtn = By.xpath("//android.widget.LinearLayout[@content-desc=\"22/03/2019\"]/android.widget.ImageView");

    String appPackage   = "com.orgname.MERBCOTIZADOR";
    String appActivity  = "com.orgname.MERBCOTIZADOR.MERBCOTIZADOR";

    //X and Y coordinate of calender button(i didn't use it)
    int x = 650;
    int y = 1200;

    //label
    String strMensajePrincipal = "Elige tu anhelo";
    String strHipotecario = "Hipotecario";
    String strFooter = "© 2019 Mercantil Banco, S.A. R.U.C: 785-502-17249 DV 97.";

    //For taking current time and date
    DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh-mm-ssaa");
    Date date = new Date();
    String currentDate = dateFormat.format(date);

    @BeforeTest
    public void setUpForAndroid() throws Exception{
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("platformName", "android");
        dc.setCapability("deviceName", "samsung");      //TODO: what should i use if testing on another device, eg. pixel?
        //App won't reset before starting test                            //Ans: device name does not matter, No problem, just keep it same, you can put any name like 'xyz',
        dc.setCapability("noReset", "false");
        //Android access permission dialog will accept automatically
        dc.setCapability("autoGrantPermissions", "true");

        //dc.setCapability("app", appPath);

        dc.setCapability("appPackage", appPackage);
        dc.setCapability("appActivity", appActivity);

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);
        System.out.println("Test Started");
    }

    //Validate Initial Screen text
    @Test
    public void testCase1_validateInitialScreenText() {
        System.out.println("TC#1: Validate Initial Screen text started");

        //Presionar Cancelar en modo debug
        waitForVisibilityOf(cancelBtn);
        driver.findElement(cancelBtn).click();
        System.out.println("Clicked on Cancel button");

        waitForVisibilityOf(lblMensajePrincipal);

        String lblMensajePrincipalTC = driver.findElement(lblMensajePrincipal).getText();
        String lblHipotecarioTC = driver.findElement(lblHipotecario).getText();

        Assert.assertEquals(lblMensajePrincipalTC,strMensajePrincipal);
        Assert.assertEquals(lblHipotecarioTC,strHipotecario);
    }

    //Validate footer text
    @Test
    public void testCase2_validateFooterText() {
        System.out.println("TC#2 Validate footer text started");

        //Presionar Hipotecario
        waitForVisibilityOf(flxHipotecario);
        driver.findElement(flxHipotecario).click();

        //Pantalla de Solicitud
        waitForVisibilityOf(nombreApellido);

        String lblMensajeFooterTC =  driver.findElement(lblMensaje).getText();

        Assert.assertEquals(lblMensajeFooterTC,strFooter);
    }


    @Test
    public void testCase3_fillUpUserInfo() throws Exception {

        System.out.println("TC#3 Fill up user info started");

        //Pantalla de Solicitud
        waitForVisibilityOf(nombreApellido);                                //Screen loaded
        driver.findElement(nombreApellido).sendKeys("Jorge Acosta");
        ((AppiumDriver)driver).hideKeyboard();
        driver.findElement(generom).click();

        driver.findElement(residencia).click();
        driver.findElement(panameno).click();              //TODO: Select: Panameno by id **Ans: there is no way to select by id, xpath is ok for this one

        driver.findElement(cedulaPasaporte).sendKeys("17125036");
        ((AppiumDriver)driver).hideKeyboard();
        driver.findElement(emailField).sendKeys("jorge.daniel@gmail.com");
        ((AppiumDriver)driver).hideKeyboard();
        driver.findElement(mobileField).sendKeys("5619908436");
        ((AppiumDriver)driver).hideKeyboard();
        Thread.sleep(2000);

        //TODO: Does not work - How to select a date on the calendar ?
        //Ans: Done

        //TODO: I'd like to select this value using an id instead of XPath
        //Ans: There is no unique element id, xpath I have used screen coordinate

        //new TouchAction((AndroidDriver)driver).tap(point(x, y)).perform();
        driver.findElement(calenderBtn).click();
        Thread.sleep(1000);

        driver.findElement(previousBtn).click();
        driver.findElement(selectOne).click();
        driver.findElement(okBtn).click();

        //For vertical scrolling the page
        swipeVertical((AppiumDriver)driver,0.9,0.1,0.5,3000);

        driver.findElement(tipoIngreso).click();
        driver.findElement(asalariado).click();            //TODO: Select: Asalariado by id
        driver.findElement(tuLetra).click();       //There is no unique id

        waitForVisibilityOf(seekBarOne);

        //TODO: how to move the sliders ?
        //Ans: Done

        //Move 1st seek bar
        //Locating seekbar using resource id, first seek bar
        WebElement seekBarOneWd = driver.findElement(seekBarOne);
        // get start co-ordinate of seekbar
        int start = seekBarOneWd.getLocation().getX();
        //Get width of seekbar
        int end = seekBarOneWd.getSize().getWidth();
        //get location of seekbar vertically
        int y = seekBarOneWd.getLocation().getY();
        // Select till which position you want to move the seekbar
        TouchAction action = new TouchAction((AndroidDriver)driver);

        action.press(point(start,y)).moveTo(point(end,y)).release().perform();
        Thread.sleep(2000);


        // move 2nd seek bar
        WebElement seekBarTwoWd = driver.findElement(seekBarTwo);
        int start2 = seekBarTwoWd.getLocation().getX();
        int end2 = seekBarTwoWd.getSize().getWidth();
        int y2 = seekBarTwoWd.getLocation().getY();
        action.press(point(start2,y2)).moveTo(point(end2,y2)).release().perform();
        Thread.sleep(2000);


        //move 3rd seek bar
        WebElement seekBarThreeWd = driver.findElement(seekBarThree);
        int start3 = seekBarThreeWd.getLocation().getX();
        int end3 = seekBarThreeWd.getSize().getWidth();
        int y3 = seekBarThreeWd.getLocation().getY();
        action.press(point(start3,y3)).moveTo(point(end3,y3)).release().perform();
        Thread.sleep(2000);

        //For vertical scrolling the page
        swipeVertical((AppiumDriver)driver,0.9,0.1,0.5,3000);
        Thread.sleep(2000);

        driver.findElement(btnCalcularLetra).click();
        waitForVisibilityOf(valorVivienda);
    }

    @AfterTest
    public void endTest(){
        System.out.println("Test End");
        driver.quit();
    }

    @AfterMethod
    public void getResult(ITestResult result) throws Exception {
        if (result.getStatus() == ITestResult.FAILURE) {
            String screenShotPath = capture(driver, result.getName() + "_" + currentDate);
        }
    }

    //This method waiting for any element max 30 sec
    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    //This method for taking screenshot
    public static String capture(WebDriver driver, String screenShotName) throws IOException {
        TakesScreenshot ts = (TakesScreenshot)driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        String dest = System.getProperty("user.dir") + "\\errorscreenshots\\" + screenShotName + ".png";
        File destination = new File(dest);
        FileUtils.copyFile(source, destination);

        return dest;
    }

    //This method for vertical scrolling
    public static void swipeVertical(AppiumDriver driver, double startPercentage, double finalPercentage, double anchorPercentage, int duration) throws Exception {
        Dimension size = driver.manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * finalPercentage);
        new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
    }
}
